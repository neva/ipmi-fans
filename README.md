# ipmi-fans

Created for Dell servers that have locked down fan curve settings from past service bought on the second-hand market. 

## Requirements

* cron
* root (could probably be run without root, untested)
* [ipmitool](https://github.com/ipmitool/ipmitool)

## Installation

1. Get the [ipmi-fans.sh](ipmi-fans.sh) script in your root directory.
1. Edit the script to include your IP, Username, and Password of the iDrak installed in your machine.
1. Test it by running the script. It should report your temperature range and set the fan speed.
1. Install the cron script to run every minute.

## Disclaimer

This could cause your system damage as it only checks temperature every minute if your CPU suddenly jumps to extremely high temperatures and one or many other safeguards fail.

**Use at your own risk.** You're running the tightrope of wife-approval and/or not requiring ear protection to be near or around your servers vs overheat conditions.

## Credits

This was derived from somewhere, but I do not have the source. Somewhere in the spiderweb of links here might contain the source of what this was derived from:

- https://www.reddit.com/r/homelab/comments/t9pa13/dell_poweredge_fan_control_with_ipmitool/
- https://www.dell.com/community/PowerEdge-Hardware-General/Dell-ENG-is-taking-away-fan-speed-control-away-from-users-iDrac/td-p/7441702
- https://old.reddit.com/r/homelab/comments/7xqb11/dell_fan_noise_control_silence_your_poweredge/
- https://www.spxlabs.com/blog/2019/3/16/silence-your-dell-poweredge-server
