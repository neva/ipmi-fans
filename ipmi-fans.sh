#!/usr/bin/env bash
set -euo pipefail

IPMIHOST="" # your idrak ip
IPMIUSER="" # idrak username
IPMIPASS="" # idrak password

MAXTEMP=85
HIGTEMP=75
MEDTEMP=65
LOWTEMP=55
MINTEMP=45

HEX10="0x$(printf '%x' 10)"
HEX20="0x$(printf '%x' 20)"
HEX30="0x$(printf '%x' 30)"
HEX40="0x$(printf '%x' 40)"
HEX50="0x$(printf '%x' 50)"
HEX60="0x$(printf '%x' 60)"
HEX70="0x$(printf '%x' 70)"
HEX80="0x$(printf '%x' 80)"
HEX90="0x$(printf '%x' 90)"
HEX100="0x$(printf '%x' 100)"

TEMP=$(ipmitool \
    -I lanplus \
    -H $IPMIHOST \
    -U $IPMIUSER \
    -P $IPMIPASS \
    sdr type temperature \
    | grep Temp \
    | grep -Po '\d{2}' \
    | tail -1)

if [[ $TEMP > $MAXTEMP ]]; then
    printf "Temp too high! Activating dynamic fan control. ($TEMP C)"
    # Activate dynamic fans
    ipmitool \
        -I lanplus \
        -H $IPMIHOST \
        -U $IPMIUSER \
        -P $IPMIPASS \
        raw 0x30 0x30 0x01 0x01
else
    printf "Temperature is ok! ($TEMP C)"
    # Activate manual fans
    ipmitool \
        -I lanplus \
        -H $IPMIHOST \
        -U $IPMIUSER \
        -P $IPMIPASS \
        raw 0x30 0x30 0x01 0x00
    if [[ $TEMP > $HIGTEMP ]]; then
        ipmitool \
            -I lanplus \
            -H $IPMIHOST \
            -U $IPMIUSER \
            -P $IPMIPASS \
            raw 0x30 0x30 0x02 0xff $HEX60
    elif [[ $TEMP > $MEDTEMP ]]; then
        ipmitool \
            -I lanplus \
            -H $IPMIHOST \
            -U $IPMIUSER \
            -P $IPMIPASS \
            raw 0x30 0x30 0x02 0xff $HEX50
    elif [[ $TEMP > $LOWTEMP ]]; then
        ipmitool \
            -I lanplus \
            -H $IPMIHOST \
            -U $IPMIUSER \
            -P $IPMIPASS \
            raw 0x30 0x30 0x02 0xff $HEX40
    elif [[ $TEMP > $MINTEMP ]]; then
        ipmitool \
            -I lanplus \
            -H $IPMIHOST \
            -U $IPMIUSER \
            -P $IPMIPASS \
            raw 0x30 0x30 0x02 0xff $HEX20
    else
        ipmitool \
            -I lanplus \
            -H $IPMIHOST \
            -U $IPMIUSER \
            -P $IPMIPASS \
            raw 0x30 0x30 0x02 0xff $HEX10
    fi
fi

